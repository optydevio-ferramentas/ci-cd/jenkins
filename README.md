![](https://i.ibb.co/YcN3QxM/optydev-io-logo-30.png)

![](https://www.jenkins.io/apple-touch-icon.png) ![](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)

# IMAGEM JENKINS PARA CI/CD de __APPs optydev.io__
 
-   [x]  __Todos os plugins instalados;__
-   [x]  Portas:  __'8080:8080' ; '443:8443' e '50000:50000';__
-   [x]  Usuário: __padrão da Startup.__

# DOCKER FILE: Dockerfile
[Dúvidas: Visal geral sobre  __Docker__](https://docs.docker.com/get-started/overview/)

Arquivo(file):__Dockerfile__
```
FROM jenkins/jenkins:lts
USER root
RUN apt-get update && \
     apt-get -y install apt-transport-https \
         ca-certificates \
         curl \
         gnupg2 \
         software-properties-common && \
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
       $(lsb_release -cs) \
       stable" && \
    apt-get update && \
    apt-get -y install docker-ce

RUN curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

RUN usermod -aG docker jenkins

USER jenkins
```

# DOCKER-COMPOSE FILE: docker-compose.yml
[Dúvidas: Como criar um __docker-compose file__](https://docs.docker.com/compose/)

-  __Opção 1__: com a imagem configurada do arquivo: __Dockerfile__ acima.

Arquivo(file): __docker-compose.yml__
```
version: '3'
services:
  optydevio-jenkins:
    build: .
    ports:
      - '8080:8080'
      - '443:8443'
      - '50000:50000'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

-  __Opção 2__: com a imagem __optydevio/jenkins:lts__ desde repositório

Arquivo(file): __docker-compose.yml__
```
version: '3'
services:
  optydevio-jenkins:
    image: 'optydevio/jenkins:lts'
    ports:
      - '8080:8080'
      - '443:8443'
      - '50000:50000'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

# COMANDOS PARA SUBIR O CONTAINER

- __Opção 1__:  usar à imagem deste repositório.

```
docker image pull optydevio/jenkins:lts
docker container run -d -p 8080:8080 443:8443 50000:50000 optydevio/jenkins:lts

```

- __Opção 2__:  usar apenas o arquivo __Dockerfile__

```
docker-image build   .
docker container run -d -p 8080:8080 443:8443 50000:50000 jenkins/jenkins:lts 

```

- __Opção 3__:  usar apenas o arquivo __docker-container.yml__  com o __Dockerfile__ no build. Criará a imagem: ___jenkins/jenkins:lts__  __(Opção 1 do DOCKER-COMPOSE FILE)___

```
docker-compose up -d

```

- __Opção 4__:  usar apenas o arquivo __docker-container.yml__  com a imagem __optydevio/jenkins:lts__.  __(Opção 2 do DOCKER-COMPOSE FILE)___

```
docker-container up -d

```

# PARANDO OS CONTAINER E EXCLUIDO AS IMAGENS

- __Opção 1__ (imagem: jenkins/jenkins:lts): 

```
docker-container down
docker image rm -f jenkins/jenkins:lts
docker container rm -f jenkins/jenkins:lts

```

- __Opção 2__ (imagem: optydevio/jenkins:lts): 

```
docker-container down
docker image rm -f optydevio/jenkins:lts
docker container rm -f optydevio/jenkins:lts

```


#### Todos os direitos reservados à __optydev.io__